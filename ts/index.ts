/// <reference path="jquery.d.ts" />

const MUSIC_URL = "http://hms.space/api/list?apiKey=K7C3EK91V7DL20AT8NS0OZARPF&chatID=872334199540444"

interface LinkResult {
  Path: string;
  TargetURL: string;
  Creator: string;
  Created: Date;
}

class MusicFrontend {
  linkList: Array<LinkResult>
  ytLinks: Array<LinkResult>
  songLinkLinks: Array<LinkResult>
  spotifyLinks: Array<LinkResult>
  unrecognized: Array<LinkResult>
  elem: JQuery

  constructor(linkList: Array<LinkResult>) {
    this.linkList = linkList;
    this.ytLinks = [];
    this.songLinkLinks = [];
    this.spotifyLinks = [];
    this.unrecognized = [];
    this.elem = $("#mainLinkList");
    this.renderLinks();


    for (var link of this.unrecognized) {
      $("#unrecog").append(`<div class='unrecognized'>${link.TargetURL}</div>`);
    }

  }

  renderLinks(): void {
    var users = {};
    for (var link of this.linkList) {
      if (link.Creator) {
        if (users[link.Creator])
          users[link.Creator] += 1;
        else
          users[link.Creator] = 1;
      }
      if (this.isYT(link)) {
        this.ytLinks.push(link);
        this.renderYT(link);
      } else if (this.isSongLink(link)) {
        this.renderSongLink(link);
      } else if (this.isSpotify(link)){
        this.renderSpotify(link);
      } else {
        this.unrecognized.push(link);
      }
    }

    for (var name of Object.keys(users)) {
      var count = users[name];
      var perc = Math.round(100 * count / this.linkList.length);
      $("#user-stats").append(
        `<tr class='user-stat'><td>${name}:</td><td>${count} (${perc}%)</td></tr>`
      );
    }

    $("#user-stats").append(
      `<tr class='user-stat stat-total'> 
      <td>Total: </td><td>${this.linkList.length} (100%)</td></tr>`
    );
  }

  isYT(link: LinkResult): boolean {
    return link.TargetURL.indexOf("youtube.com") != -1 || link.TargetURL.indexOf("youtu.be") != -1;
  }

  isSongLink(link: LinkResult): boolean {
    return link.TargetURL.indexOf("songl.ink") != -1;
  }

  isSpotify(link: LinkResult): boolean {
    return link.TargetURL.indexOf("spotify.com") != -1;
  }

  renderYT(link: LinkResult): void {
    this.elem.append($(`<div class='youtube'>
                       <a target="_blank" href="${link.TargetURL}">${link.TargetURL}</a> by ${link.Creator}</div>`));
    if (this.ytLinks.length > 5)
      return;
    var ytIDRegex = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;

    var match = link.TargetURL.match(ytIDRegex);
    if (!match) {
      return;
    }

    var ytID = match[7];
    this.elem.append(`<iframe src="https://www.youtube.com/embed/${ytID}" width="720" height="405"></iframe>`);
    
  }

  renderSongLink(link: LinkResult): void {
    this.elem.append($(`<div class='songlink'>
                       <a target="_blank" href="${link.TargetURL}">${link.TargetURL}</a>
                       by ${link.Creator}</div>`))
  }

  renderSpotify(link: LinkResult): void {
    this.elem.append($(`<div class='spotify'><a target="_blank" href="${link.TargetURL}">
                       ${link.TargetURL}</a> by ${link.Creator}</div>`))
  }
}


$(function() {
  var App;
  $.get(MUSIC_URL, function(respJSON: any) {
    if (!respJSON.Success) {
      $("#loadError").show();
    } else {
      App = new MusicFrontend(respJSON.Links);
    }
  })
})
