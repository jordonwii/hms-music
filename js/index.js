/// <reference path="jquery.d.ts" />
var MUSIC_URL = "http://hms.space/api/list?apiKey=K7C3EK91V7DL20AT8NS0OZARPF&chatID=872334199540444";
var MusicFrontend = (function () {
    function MusicFrontend(linkList) {
        this.linkList = linkList;
        this.ytLinks = [];
        this.songLinkLinks = [];
        this.spotifyLinks = [];
        this.unrecognized = [];
        this.elem = $("#mainLinkList");
        this.renderLinks();
        for (var _i = 0, _a = this.unrecognized; _i < _a.length; _i++) {
            var link = _a[_i];
            $("#unrecog").append("<div class='unrecognized'>" + link.TargetURL + "</div>");
        }
    }
    MusicFrontend.prototype.renderLinks = function () {
        var users = {};
        for (var _i = 0, _a = this.linkList; _i < _a.length; _i++) {
            var link = _a[_i];
            if (link.Creator) {
                if (users[link.Creator])
                    users[link.Creator] += 1;
                else
                    users[link.Creator] = 1;
            }
            if (this.isYT(link)) {
                this.ytLinks.push(link);
                this.renderYT(link);
            }
            else if (this.isSongLink(link)) {
                this.renderSongLink(link);
            }
            else if (this.isSpotify(link)) {
                this.renderSpotify(link);
            }
            else {
                this.unrecognized.push(link);
            }
        }
        for (var _b = 0, _c = Object.keys(users); _b < _c.length; _b++) {
            var name = _c[_b];
            var count = users[name];
            var perc = Math.round(100 * count / this.linkList.length);
            $("#user-stats").append("<tr class='user-stat'><td>" + name + ":</td><td>" + count + " (" + perc + "%)</td></tr>");
        }
        $("#user-stats").append("<tr class='user-stat stat-total'> \n      <td>Total: </td><td>" + this.linkList.length + " (100%)</td></tr>");
    };
    MusicFrontend.prototype.isYT = function (link) {
        return link.TargetURL.indexOf("youtube.com") != -1 || link.TargetURL.indexOf("youtu.be") != -1;
    };
    MusicFrontend.prototype.isSongLink = function (link) {
        return link.TargetURL.indexOf("songl.ink") != -1;
    };
    MusicFrontend.prototype.isSpotify = function (link) {
        return link.TargetURL.indexOf("spotify.com") != -1;
    };
    MusicFrontend.prototype.renderYT = function (link) {
        this.elem.append($("<div class='youtube'>\n                       <a target=\"_blank\" href=\"" + link.TargetURL + "\">" + link.TargetURL + "</a> by " + link.Creator + "</div>"));
        if (this.ytLinks.length > 5)
            return;
        var ytIDRegex = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
        var match = link.TargetURL.match(ytIDRegex);
        if (!match) {
            return;
        }
        var ytID = match[7];
        this.elem.append("<iframe src=\"https://www.youtube.com/embed/" + ytID + "\" width=\"720\" height=\"405\"></iframe>");
    };
    MusicFrontend.prototype.renderSongLink = function (link) {
        this.elem.append($("<div class='songlink'>\n                       <a target=\"_blank\" href=\"" + link.TargetURL + "\">" + link.TargetURL + "</a>\n                       by " + link.Creator + "</div>"));
    };
    MusicFrontend.prototype.renderSpotify = function (link) {
        this.elem.append($("<div class='spotify'><a target=\"_blank\" href=\"" + link.TargetURL + "\">\n                       " + link.TargetURL + "</a> by " + link.Creator + "</div>"));
    };
    return MusicFrontend;
}());
$(function () {
    var App;
    $.get(MUSIC_URL, function (respJSON) {
        if (!respJSON.Success) {
            $("#loadError").show();
        }
        else {
            App = new MusicFrontend(respJSON.Links);
        }
    });
});
